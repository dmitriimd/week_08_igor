package ru.edu;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class MockContext {
    private DataSource dataSource;

    public MockContext(DataSource dataSourceMock) throws NamingException {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.osjava.sj.SimpleContextFactory");
        System.setProperty("org.osjava.sj.jndi.shared", "true");

        Context ic = new InitialContext();
        ic.unbind("java:/comp/env");
        ic.unbind("java:/comp/env/jdbc/dbLink");
        ic.unbind("jdbc/dbLink");

        ic.bind("java:/comp/env", ic);
        ic.bind("java:/comp/env/jdbc/dbLink", dataSourceMock);
        ic.bind("jdbc/dbLink", dataSourceMock);

        Context ctx = new InitialContext();
        Context env = (Context) ctx.lookup("java:/comp/env");
        dataSource = (DataSource) env.lookup("jdbc/dbLink");
    }

    public DataSource getDataSource() {
        synchronized (MockContext.class) {
            return dataSource;
        }
    }
}
