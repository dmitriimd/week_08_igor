package ru.edu.db;

import liquibase.pro.packaged.M;
import org.junit.Before;
import org.junit.Test;
import ru.edu.MockContext;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CRUDTest {
    private static final String
            SELECT_ALL_SQL = "SELECT * FROM adverts";
    private static final String
            SELECT_BY_ID = "SELECT * FROM adverts WHERE id=?";
    private static final String
            INSERT_SQL = "INSERT INTO adverts "
            + "(title, type, text, price, "
            + "publisher, email, phone, picture_url) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String
            UPDATE_SQL = "UPDATE adverts SET "
            + "title=?, type=?, text=?, price=?, "
            + "publisher=?, email=?, phone=?, picture_url=? "
            + "WHERE id=?";


    private DataSource dataSourceMock = mock(DataSource.class);
    private Connection connectionMock = mock(Connection.class);

    private PreparedStatement statementAll = mock(PreparedStatement.class);
    private PreparedStatement statementById = mock(PreparedStatement.class);
    private PreparedStatement statementInsert = mock(PreparedStatement.class);
    private PreparedStatement statementUpdate = mock(PreparedStatement.class);

    private ResultSet resultSetAll = mock(ResultSet.class);
    private ResultSet resultSetById = mock(ResultSet.class);
    private ResultSet resultSetInsert = mock(ResultSet.class);
    private ResultSet resultSetUpdate = mock(ResultSet.class);

    private Advert advert = mock(Advert.class);
    private Advert advert2 = mock(Advert.class);
    private Advert advertInsert = mock(Advert.class);
    private Advert advertUpdate = mock(Advert.class);

    private DataSource dataSource;
    private CRUD crud;

    @Before
    public void setupJndi() throws NamingException, SQLException, InterruptedException {
        MockContext mockContext = new MockContext(dataSourceMock);
        dataSource = mockContext.getDataSource();
        assertEquals(dataSourceMock, dataSource);

        when(dataSource.getConnection()).thenReturn(connectionMock);

        when(dataSource.getConnection().prepareStatement(SELECT_ALL_SQL)).thenReturn(statementAll);
        when(statementAll.executeQuery()).thenReturn(resultSetAll);
        when(resultSetAll.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSetAll.getLong("id")).thenReturn(1l).thenReturn(2l);
        when(resultSetAll.getString("title")).thenReturn("title1").thenReturn("title2");
        when(resultSetAll.getString("type")).thenReturn("type1").thenReturn("type2");
        when(resultSetAll.getString("price")).thenReturn("10").thenReturn("100");
        when(resultSetAll.getString("text")).thenReturn("text1").thenReturn("text2");
        when(resultSetAll.getString("publisher")).thenReturn("publisher1").thenReturn("publisher2");
        when(resultSetAll.getString("email")).thenReturn("email1").thenReturn("email2");
        when(resultSetAll.getString("phone")).thenReturn("phone1").thenReturn("phone2");
        when(resultSetAll.getString("picture_url")).thenReturn("picture_url1").thenReturn("picture_url2");

        when(dataSource.getConnection().prepareStatement(SELECT_BY_ID)).thenReturn(statementById);
        when(statementById.executeQuery()).thenReturn(resultSetById);
        when(resultSetById.next()).thenReturn(true).thenReturn(false);
        when(resultSetById.getLong("id")).thenReturn(1l);
        when(resultSetById.getString("title")).thenReturn("title1");
        when(resultSetById.getString("type")).thenReturn("type1");
        when(resultSetById.getString("price")).thenReturn("10");
        when(resultSetById.getString("text")).thenReturn("text1");
        when(resultSetById.getString("publisher")).thenReturn("publisher1");
        when(resultSetById.getString("email")).thenReturn("email1");
        when(resultSetById.getString("phone")).thenReturn("phone1");
        when(resultSetById.getString("picture_url")).thenReturn("picture_url1");

        when(dataSource.getConnection().prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS)).thenReturn(statementInsert);
        when(statementInsert.getGeneratedKeys()).thenReturn(resultSetInsert);
        when(resultSetInsert.next()).thenReturn(true).thenReturn(false);

        when(dataSource.getConnection().prepareStatement(UPDATE_SQL, Statement.RETURN_GENERATED_KEYS)).thenReturn(statementUpdate);
        when(statementUpdate.getGeneratedKeys()).thenReturn(resultSetUpdate);
        when(resultSetUpdate.next()).thenReturn(true).thenReturn(false);
        when(resultSetUpdate.getLong(1)).thenReturn(2L);

        CRUD.setOverride(null);
        crud = CRUD.getInstance();

    }

    @Test
    public void getInstance() {
        assertNotNull(crud);
    }

    @Test
    public void testGetIndex() throws SQLException {
        List<Advert> adverts = crud.getIndex();

        assertEquals(2, adverts.size());
        assertEquals(Long.valueOf(1), adverts.get(0).getId());
        assertEquals("title1", adverts.get(0).getTitle());
        assertEquals("type1", adverts.get(0).getType());
        assertEquals(new BigDecimal("10"), adverts.get(0).getPrice());
        assertEquals("text1", adverts.get(0).getText());
        assertEquals("publisher1", adverts.get(0).getPublisher());
        assertEquals("email1", adverts.get(0).getEmail());
        assertEquals("phone1", adverts.get(0).getPhone());
        assertEquals("picture_url1", adverts.get(0).getPictureUrl());
        assertEquals("te...", adverts.get(0).getShort(2));

        assertEquals("title2", adverts.get(1).getTitle());
    }

    @Test
    public void testGetById() throws SQLException {
        advert2 = crud.getById("1");
        assertEquals("title1", advert2.getTitle());
    }

    @Test
    public void testSaveInsert() throws SQLException {
        when(advertInsert.getId()).thenReturn(null);
        when(advertInsert.getPrice()).thenReturn(new BigDecimal(10));
        Long id = crud.save(advertInsert);
        assertEquals(Long.valueOf(0), id);
    }

    @Test
    public void testSaveUpdate() throws SQLException {
        Long id = crud.save(advertUpdate);
        assertEquals(Long.valueOf(2), id);
    }
}