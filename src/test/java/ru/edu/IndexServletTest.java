package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.CRUD;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class IndexServletTest {
    private final String PAGE = "/WEB-INF/index.jsp";
    private final String ID_PARAM_MISSING = "/index?error=id_param_missing";

    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);

    private DataSource dataSourceMock = mock(DataSource.class);
    private Context context = mock(Context.class);

    private DataSource dataSource;
    private IndexServlet index;
    private CRUD crud;

    @Before
    public void setup() throws NamingException {
        MockContext mockContext = new MockContext(dataSourceMock);
        dataSource = mockContext.getDataSource();

        assertEquals(dataSource, dataSourceMock);

        CRUD.setOverride(mock(CRUD.class));
        crud = CRUD.getInstance();

        index = new IndexServlet();
    }

    @Test
    public void testDoGetWithError() throws ServletException, IOException {
        when(request.getParameter("error")).thenReturn(null);

        index.doGet(request, response);

        verify(response, times(1)).sendRedirect(ID_PARAM_MISSING);

    }

}