<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ page import="ru.edu.db.Advert" %>
<html>
<head>
    <!-- https://fonts.google.com/specimen/Roboto -->
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <link href="./style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="header">
    <h1>Доска объявлений</h1>
</div>
<%
    Advert adv = (Advert) request.getAttribute("advert");
%>
<%@ include file="/WEB-INF/templates/breadcrumbs.jsp" %>
<div class="content">
    <div class="advert">
        <div class="common-info">
            <img src="<%=adv.getPictureUrl()%>"/>
            <h2><%=adv.getTitle()%></h2>
            <h3><%=adv.getType()%></h3>
            <div class="price">
                <%=adv.getPrice().toPlainString()%> руб.
            </div>
        </div>
        <div class="adv-text">
            <h4>Текст объявления</h4>
            <p><%=adv.getText()%></p>
        </div>
        <div class="adv-contacts">
            <h4>Контактные данные</h4>
            <p>Автор: <span><%=adv.getPublisher()%></span></p>
            <p>Email: <span><%=adv.getEmail()%></span></p>
            <p>Телефон: <span><%=adv.getPhone()%></span></p>
        </div>
    </div>
    <% if (request.getAttribute("auth").equals(true)) { %>
    <div class="edit-btn">
        <a href="edit?id=<%=adv.getId()%>">Редактировать</a>
    </div>
    <% } %>
</div>
<div class="footer">
    <a href="about">Об авторе</a>
</div>
</body>
</html>