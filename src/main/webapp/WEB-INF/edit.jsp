<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ page import="ru.edu.db.Advert" %>
<%
    Advert advert = (Advert) request.getAttribute("advert");
    boolean isEdit = advert != null;
%>
<html>
<jsp:include page="/WEB-INF/templates/header.jsp">
    <jsp:param name="title" value="Edit"/>
</jsp:include>
<body>
<div class="page">
    <div class="header">
        <h1>Доска объявлений</h1>
    </div>
    <%@ include file="/WEB-INF/templates/breadcrumbs.jsp" %>
    <form method="post">
        <div class="content edit-form">
            <div class="form-item">
                <label for="title">Заголовок:</label>
                <input id="title" name="title" placeholder="Заголовок" value="<%=(isEdit ? advert.getTitle(): "")%>"/>
            </div>
            <div class="form-item">
                <label for="type">Тип услуги:</label>
                <select id="type" name="type" placeholder="Тип">
                    <option
                    "<%=isEdit && "Услуги".equals(advert.getType()) ? "selected" : ""%>">Услуги</option>
                    <option
                    "<%=isEdit && "Покупка".equals(advert.getType()) ? "selected" : ""%>">Покупка</option>
                    <option
                    "<%=isEdit && "Продажа".equals(advert.getType()) ? "selected" : ""%>">Продажа</option>
                </select>
            </div>
            <div class="form-item">
                <label for="text">Текст объявления:</label>
                <textarea id="text" name="text"
                          placeholder="Текст объявления"><%=(isEdit ? advert.getText() : "")%></textarea>
            </div>
            <div class="form-item">
                <label for="price">Цена:</label>
                <input id="price" name="price" placeholder="Цена"
                       value="<%=(isEdit ? advert.getPrice().toPlainString(): "")%>"/>
            </div>
            <div class="form-item">
                <label for="publisher">Автор:</label>
                <input id="publisher" name="publisher" placeholder="Автор"
                       value="<%=(isEdit ? advert.getPublisher(): "")%>"/>
            </div>
            <div class="form-item">
                <label for="email">Email:</label>
                <input id="email" name="email" placeholder="Email" value="<%=(isEdit ? advert.getEmail(): "")%>"/>
            </div>
            <div class="form-item">
                <label for="phone">Телефон:</label>
                <input id="phone" name="phone" placeholder="Телефон" value="<%=(isEdit ? advert.getPhone(): "")%>"/>
            </div>
            <div class="form-item">
                <label for="picture_url">Изображение:</label>
                <input id="picture_url" name="picture_url" placeholder="Изображение"
                       value="<%=(isEdit ? advert.getPictureUrl(): "")%>"/>
            </div>
        </div>
        <div class="button-container">
            <button class="send-button">Разместить объявление</button>
        </div>
    </form>
    <%@ include file="/WEB-INF/templates/footer.jsp" %>
</div>
</body>
</html>