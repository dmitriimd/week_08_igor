package ru.edu;

import ru.edu.db.CRUD;
import ru.edu.db.Advert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

public class EditServlet extends HttpServlet {

    /**
     * Подключаем синглтон.
     */
    private CRUD crud = CRUD.getInstance();

    /**
     * Пример.
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {
        String id = req.getParameter("id");
        Advert advert = null;
        if (id != null) {
            advert = crud.getById(id);
        }
        req.setAttribute("advert", advert);
        resp.setCharacterEncoding("UTF-8");
        req.getRequestDispatcher("/WEB-INF/edit.jsp").forward(req, resp);
    }

    /**
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    @Override
    protected void doPost(final HttpServletRequest req,
                          final HttpServletResponse resp)
            throws IOException {
        Map form = req.getParameterMap();
        Advert advert = new Advert();
        if (form.containsKey("id")) {
            advert.setId(Long.parseLong(getStr(form, "id")));
        }
        advert.setTitle(getStr(form, "title"));
        advert.setType(getStr(form, "type"));
        advert.setText(getStr(form, "text"));
        advert.setPrice(new BigDecimal(getStr(form, "price")));
        advert.setPublisher(getStr(form, "publisher"));
        advert.setEmail(getStr(form, "email"));
        advert.setPhone(getStr(form, "phone"));
        advert.setPictureUrl(getStr(form, "picture_url"));

        if (advert.getTitle() == null || advert.getTitle().isEmpty()) {
            resp.sendRedirect("view?id=" + advert.getId());
            return;
        }
        Long id = crud.save(advert);
        advert.setId(id);
        resp.sendRedirect("edit?status=ok&id=" + advert.getId());
    }

    private String getStr(final Map form, final String id) {
        return ((String[]) form.get(id))[0];
    }

}
