package ru.edu.db;

import java.math.BigDecimal;

public class Advert {
    /**
     * ID.
     */
    private Long id;
    /**
     * Заголовок.
     */
    private String title;
    /**
     * Тип объявления.
     */
    private String type;
    /**
     * Цена.
     */
    private BigDecimal price;
    /**
     * Текст объявления.
     */
    private String text;
    /**
     * Автор публикации.
     */
    private String publisher;
    /**
     * Почта.
     */
    private String email;
    /**
     * Телефон.
     */
    private String phone;
    /**
     * Url картинки.
     */
    private String pictureUrl;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param advertId
     */
    public void setId(final Long advertId) {
        this.id = advertId;
    }

    /**
     * @return заголовок
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param advertTitle
     */
    public void setTitle(final String advertTitle) {
        this.title = advertTitle;
    }

    /**
     * @return тип объявления
     */
    public String getType() {
        return type;
    }

    /**
     * @param advertType
     */
    public void setType(final String advertType) {
        this.type = advertType;
    }

    /**
     * @return цена
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param advertPrice
     */
    public void setPrice(final BigDecimal advertPrice) {
        this.price = advertPrice;
    }

    /**
     * @return текст объявления
     */
    public String getText() {
        return text;
    }

    /**
     * @param advertText
     */
    public void setText(final String advertText) {
        this.text = advertText;
    }

    /**
     * @return автор публикации
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * @param advertPublisher
     */
    public void setPublisher(final String advertPublisher) {
        this.publisher = advertPublisher;
    }

    /**
     * @return почта
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param advertEmail
     */
    public void setEmail(final String advertEmail) {
        this.email = advertEmail;
    }

    /**
     * @return телефон
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param advertPhone
     */
    public void setPhone(final String advertPhone) {
        this.phone = advertPhone;
    }

    /**
     * @return url картинки
     */
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     * @param advertPictureUrl
     */
    public void setPictureUrl(final String advertPictureUrl) {
        this.pictureUrl = advertPictureUrl;
    }

    /**
     * @param length
     * @return короткое описание объявления
     */
    public String getShort(final int length) {
        if (text.length() <= length) {
            return text;
        }
        return text.substring(0, length) + "...";
    }
}
