package ru.edu.filter;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

public class AuthFilter implements Filter {
    /**
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(final FilterConfig filterConfig) {
    }

    /**
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) request;

        if (httpReq.getServletPath().contains("/index")
                || httpReq.getServletPath().contains("/view")) {
            httpReq.setAttribute("auth", isAuth(httpReq));
        }
        if (!httpReq.getServletPath().contains("/edit")) {
            chain.doFilter(request, response);
            return;
        }
        Cookie[] cookies = httpReq.getCookies();
        if (cookies != null && cookieIsValid(cookies)) {
            chain.doFilter(request, response);
            return;
        }

        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setCharacterEncoding("UTF-8");
        httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN,
                "Not authorized");
    }

    /**
     * Проверка на авторизацию.
     *
     * @param req
     * @return true/false
     */
    private boolean isAuth(final HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null && cookieIsValid(cookies)) {
            return true;
        }
        return false;
    }

    /**
     * Проверка валидности cookie.
     *
     * @param cookies
     * @return true/false
     */
    private boolean cookieIsValid(final Cookie[] cookies) {
        Optional<Cookie> authCookie = Arrays.stream(cookies)
                .filter(cookie -> "authCookie".equals(cookie.getName()))
                .findFirst();
        return authCookie.isPresent();
    }

    /**
     *
     */
    @Override
    public void destroy() {

    }
}
